from argparse import ArgumentParser
from binance_simulation import BinanceSimulation
from binance_bot import BinanceBot

def getBot(botType:str, apiFilePath:str):
    if botType == "default":
        bot = BinanceBot
    elif botType == "hardcoded":
        from hardcoded_bot import HardcodedBot
        bot = HardcodedBot
    elif botType == "random":
        from random_bot import RandomBot
        bot = RandomBot
    else:
        assert False, "Unknown bot type: %s" % botType
    return bot(apiFilePath)

def getArgs():
    parser = ArgumentParser()
    # Required params
    parser.add_argument("--botType", required=True)
    parser.add_argument("--apiFilePath", required=True)
    parser.add_argument("--mode", required=True)
    # Optional params
    parser.add_argument("--lag", type=float)
    parser.add_argument("--endTime", type=int)
    parser.add_argument("--plotFilePath")

    args = parser.parse_args()
    assert args.mode in ("simulation", )
    if args.mode == "simulation":
        assert not args.lag is None
        args.endTime = 1<<31 if not args.endTime else args.endTime
    return args

def main():
    args = getArgs()
    bot = getBot(args.botType, args.apiFilePath)

    simulation = BinanceSimulation(bot, lag=args.lag, endTime=args.endTime, plotFilePath=args.plotFilePath)
    print(simulation)
    simulation.run()

if __name__ == "__main__":
    main()