from binance_bot import BinanceBot
from overrides import overrides

class HardcodedBot(BinanceBot):
    def __init__(self, apiFilePath:str):
        super().__init__(apiFilePath)
        self.t = 0

    @overrides
    def makeNewActions(self, balance, prices):
        self.t += 1
        if self.t == 1:
            return [
                ("SELL", "BTT", "BUSD", 100000, None),
            ]
        
        if self.t == 2:
            return [
                ("BUY", "BTC", "BUSD", 100000 * prices["BTTBUSD"] / prices["BTCBUSD"], None)
            ]

        if self.t == 3:
            return [
                ("SELL", "BTT", "BUSD", 100000, None),
            ]
        
        if self.t == 4:
            return [
                ("BUY", "ETH", "BUSD", 50000 * prices["BTTBUSD"] / prices["ETHBUSD"], None),
                ("BUY", "ADA", "BUSD", 50000 * prices["BTTBUSD"] / prices["ADABUSD"], None)
            ]

        if self.t == 10:
            return self.sellAll(balance)

        return [("PASS", None, None, None, None)]

    def __str__(self):
        Str = "[Hardcoded Bot]"
        Str += "\n - Api file path: %s" % self.apiFilePath
        return Str